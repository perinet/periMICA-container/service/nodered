module gitlab.com/perinet/periMICA-container/service/nodered

go 1.22

require (
	gitlab.com/perinet/generic/apiservice/dnssd v1.0.0-rc3.0.20240924112815-5fa4955597e1
	gitlab.com/perinet/generic/apiservice/staticfiles v0.0.0-20240621123748-68fb070dfc7d
	gitlab.com/perinet/generic/lib/httpserver v1.0.1-0.20240916100425-406ce7a80d39
	gitlab.com/perinet/generic/lib/utils v1.0.1-0.20240621122644-d12809863382
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v1.0.1
	gitlab.com/perinet/periMICA-container/apiservice/network v1.0.0
	gitlab.com/perinet/periMICA-container/apiservice/node v1.0.4
	gitlab.com/perinet/periMICA-container/apiservice/proxy v0.0.0-20240624145312-c725713d9792
	gitlab.com/perinet/periMICA-container/apiservice/security v1.0.0-rc1
	gitlab.com/perinet/periMICA-container/apiservice/ssh v1.0.0
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.2-0.20240210093433-b8dc0fc11e7e // indirect
	golang.org/x/exp v0.0.0-20240613232115-7f521ea00fb8 // indirect
)
